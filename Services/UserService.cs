#nullable enable

using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using SuperQuery.Models;
using SuperQuery.Data;

namespace SuperQuery.Services
{
    internal sealed class UserService
    {
        public async Task<List<User>> GetUsersWithTasksOrderedByFirstName()
        {
            List<User> users = await Database.GetUsers();
            List<TaskModel> tasks = await Database.GetRawTasks();

            return users
                .GroupJoin<User, TaskModel, int, User>(
                    tasks,
                    u => u.Id,
                    t => t.PerformerId,
                    (u, tasks) => new User
                    {
                        Id = u.Id,
                        TeamId = u.TeamId,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        Email = u.Email,
                        RegisteredAt = u.RegisteredAt,
                        BirthDay = u.BirthDay,
                        Tasks = tasks.OrderByDescending(t => t.Name.Length),
                    }
                )
                .OrderBy(u => u.FirstName)
                .ToList();
        }

        public async Task<UserAdditionalInfo> GetAdditionalInfo(int userId)
        {
            User user = await Database.GetUser(userId);
            User[] userAsArray = new User[] { user };
            List<Project> projects = await Database.GetProjects();

            List<TaskModel> tasks = await Database
                .GetRawTasks();

            tasks = tasks
                .Select(t => new TaskModel
                {
                    Id = t.Id,
                    ProjectId = t.ProjectId,
                    PerformerId = t.PerformerId,
                    Performer = user,
                    Name = t.Name,
                    Description = t.Description,
                    State = t.State,
                    CreatedAt = t.CreatedAt,
                    FinishedAt = t.FinishedAt,
                })
                .ToList();

            return userAsArray
                .GroupJoin<User, Project, int, UserAdditionalInfo>(
                    projects,
                    u => u.Id,
                    p => p.AuthorId,
                    (u, projects) =>
                    {
                        Project? lastProject;

                        try
                        {
                            lastProject = projects.OrderByDescending(p => p.CreatedAt).First();
                        }
                        catch
                        {
                            lastProject = null;
                        }

                        return new UserAdditionalInfo
                        {
                            User = u,
                            LastProject = lastProject,
                            LastProjectTasksCount = lastProject.HasValue ? lastProject.Value.Tasks.Count() : 0,
                        };
                    }
                )
                .GroupJoin<UserAdditionalInfo, TaskModel, int, UserAdditionalInfo>(
                    tasks,
                    info => info.User.Id,
                    t => t.PerformerId,
                    (info, tasks) =>
                    {
                        TaskModel? longestTask;

                        try
                        {
                            longestTask = tasks
                                .Where(t => t.FinishedAt.HasValue)
                                .OrderByDescending(t => t.FinishedAt!.Value - t.CreatedAt)
                                .First();
                        }
                        catch
                        {
                            longestTask = null;
                        }

                        return new UserAdditionalInfo
                        {
                            User = info.User,
                            LastProject = info.LastProject,
                            LastProjectTasksCount = info.LastProjectTasksCount,
                            UnfinishedTasksCount = tasks
                                .Where(t => !t.FinishedAt.HasValue)
                                .Count(),
                            LongestTask = longestTask,
                        };
                    }
                )
                .First();
        }
    }
}
