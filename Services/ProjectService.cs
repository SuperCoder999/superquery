using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using SuperQuery.Models;
using SuperQuery.Data;

namespace SuperQuery.Services
{
    internal sealed class ProjectService
    {
        public async Task<Dictionary<Project, int>> GetProjectsTasksCountByUserId(int userId)
        {
            List<Project> projects = await Database.GetProjects();

            IEnumerable<KeyValuePair<Project, int>> pairs = projects
                    .Where(p => p.AuthorId == userId)
                    .Select(p => GetProjectTasksCountKeyValuePair(p));

            return new Dictionary<Project, int>(pairs);
        }

        public async Task<ProjectAdditionalInfo> GetAdditionalInfo(int id)
        {
            Project project = await Database.GetProject(id);
            Project[] projectAsArray = new Project[] { project };

            return projectAsArray
                .Select(p => new ProjectAdditionalInfo
                {
                    Project = p,
                    LongestDescriptionTask = p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                    ShortestNameTask = p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                    UsersCountInTeam =
                        p.Description.Length > 20 || p.Tasks.Count() < 3
                        ? p.Team.Users.Count()
                        : null
                })
                .First();
        }

        private KeyValuePair<Project, int> GetProjectTasksCountKeyValuePair(Project project)
        {
            return new KeyValuePair<Project, int>(project, project.Tasks.Count());
        }
    }
}
