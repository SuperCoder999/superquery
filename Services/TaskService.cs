using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using SuperQuery.Models;
using SuperQuery.Data;

namespace SuperQuery.Services
{
    internal sealed class TaskService
    {
        public async Task<List<TaskModel>> GetAssignedToWithShortName(int userId)
        {
            List<TaskModel> tasks = await Database.GetTasks();

            return tasks.Where(t => t.Name.Length < 45 && t.PerformerId == userId).ToList();
        }

        public async Task<List<(int, string)>> GetShortFinishedInCurrentYearAssignedTo(int userId)
        {
            List<TaskModel> tasks = await Database.GetTasks();

            return tasks
                .Where(t =>
                    t.FinishedAt.HasValue &&
                    t.FinishedAt.Value.Year == DateTime.Now.Year &&
                    t.PerformerId == userId)
                .Select(t => (t.Id, t.Name))
                .ToList();
        }
    }
}
