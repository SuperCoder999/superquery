using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using SuperQuery.Models;
using SuperQuery.Data;

namespace SuperQuery.Services
{
    internal sealed class TeamService
    {
        public async Task<List<(int, string, List<User>)>> GetTeamIdsNamesAndUsersOrderedByRegistrationDateGroupedByTeamId()
        {
            List<Team> teams = await Database.GetTeams();

            return teams
                .Where(t => t.Users.All(u => DateTime.Now.Year - u.BirthDay.Year > 10))
                .Select(t => (t.Id, t.Name, t.Users.OrderByDescending(u => u.RegisteredAt).ToList()))
                .ToList();
        }
    }
}
