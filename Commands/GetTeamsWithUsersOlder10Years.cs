using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using SuperQuery.Services;
using SuperQuery.Models;

namespace SuperQuery.Commands
{
    [Command(4, "getTeamsWithUsersOlder10Years")]
    internal sealed class GetTeamsWithUsersOlder10Years : BaseCommand<GetTeamsWithUsersOlder10Years>
    {
        private readonly TeamService teamService = new TeamService();

        public override async Task Invoke()
        {
            List<(int, string, List<User>)> data =
                await teamService.GetTeamIdsNamesAndUsersOrderedByRegistrationDateGroupedByTeamId();

            foreach ((int, string, List<User>) dataPiece in data)
            {
                var (id, name, users) = dataPiece;
                Console.WriteLine($"Id: {id}, Name: {name}, Users:");

                foreach (User user in users)
                {
                    Console.WriteLine($"  {user.FirstName} {user.LastName}");
                }

                Console.WriteLine();
            }
        }
    }
}
