using System;
using System.Threading.Tasks;
using SuperQuery.Services;
using SuperQuery.Models;

namespace SuperQuery.Commands
{
    [Command(7, "getProjectInfo")]
    [UseParameter("projectId", "projectId", "invalidNumber")]
    internal sealed class GetProjectInfo : BaseCommand<GetProjectInfo>
    {
        private readonly ProjectService projectService = new ProjectService();

        public override async Task Invoke()
        {
            int projectId = GetParameterValue<int>("projectId");
            ProjectAdditionalInfo info = await projectService.GetAdditionalInfo(projectId);

            string computedUsersCount = info.UsersCountInTeam.HasValue
                ? info.UsersCountInTeam.Value.ToString()
                : "Project doesn't meet the requirements";

            Console.WriteLine($"Project: {info.Project.Name}");
            Console.WriteLine($"Longest description task: {info.LongestDescriptionTask.Name}");
            Console.WriteLine($"Shortest name task: {info.ShortestNameTask.Name}");
            Console.WriteLine($"Users count in team: {computedUsersCount}");
        }
    }
}
