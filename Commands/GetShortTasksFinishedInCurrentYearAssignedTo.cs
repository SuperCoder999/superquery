using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SuperQuery.Services;

namespace SuperQuery.Commands
{
    [Command(3, "getShortTasksFinishedInCurrentYearAndAssignedTo")]
    [UseParameter("userId", "userId", "invalidNumber")]
    internal sealed class GetShortTasksFinishedInCurrentYearAssignedTo
        : BaseCommand<GetShortTasksFinishedInCurrentYearAssignedTo>
    {
        private readonly TaskService taskService = new TaskService();

        public override async Task Invoke()
        {
            int userId = GetParameterValue<int>("userId");
            List<(int, string)> data = await taskService.GetShortFinishedInCurrentYearAssignedTo(userId);

            foreach ((int, string) dataPiece in data)
            {
                var (id, name) = dataPiece;
                Console.WriteLine($"Id: {id}, Name: {name}");
            }
        }
    }
}
