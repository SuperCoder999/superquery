using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SuperQuery.Services;
using SuperQuery.Models;

namespace SuperQuery.Commands
{
    [Command(1, "getProjectsTasksByUserId")]
    [UseParameter("userId", "userId", "invalidNumber")]
    internal sealed class GetProjectsTasksCountByUserId : BaseCommand<GetProjectsTasksCountByUserId>
    {
        private readonly ProjectService projectService = new ProjectService();

        public override async Task Invoke()
        {
            int userId = GetParameterValue<int>("userId");
            Dictionary<Project, int> projects = await projectService.GetProjectsTasksCountByUserId(userId);

            foreach (KeyValuePair<Project, int> pair in projects)
            {
                Console.WriteLine($"Project {pair.Key.Name} has {pair.Value} tasks");
            }
        }
    }
}
