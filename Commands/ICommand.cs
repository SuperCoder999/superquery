using System.Threading.Tasks;

namespace SuperQuery.Commands
{
    internal interface ICommand
    {
        int Index { get; }
        Task Invoke();
        void Reset();
        string ToString();
    }
}
