using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SuperQuery.Services;
using SuperQuery.Models;

namespace SuperQuery.Commands
{
    [Command(2, "getAssignedTasks")]
    [UseParameter("userId", "userId", "invalidNumber")]
    internal sealed class GetAssignedTasks : BaseCommand<GetAssignedTasks>
    {
        private readonly TaskService taskService = new TaskService();

        public override async Task Invoke()
        {
            int userId = GetParameterValue<int>("userId");
            List<TaskModel> tasks = await taskService.GetAssignedToWithShortName(userId);

            foreach (TaskModel task in tasks)
            {
                Console.WriteLine($"Task '{task.Name}' with state '{task.State}'");
            }
        }
    }
}
