using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SuperQuery.Services;
using SuperQuery.Models;

namespace SuperQuery.Commands
{
    [Command(5, "getUsersOrderedByFirstNameWithTasks")]
    internal sealed class GetUsersOrderedByFirstNameWithTasks
        : BaseCommand<GetUsersOrderedByFirstNameWithTasks>
    {
        private readonly UserService userService = new UserService();

        public override async Task Invoke()
        {
            List<User> users = await userService.GetUsersWithTasksOrderedByFirstName();

            foreach (User user in users)
            {
                Console.WriteLine($"{user.FirstName} {user.LastName}:");

                foreach (TaskModel task in user.Tasks)
                {
                    Console.WriteLine($"  {task.Name}");
                }

                Console.WriteLine();
            }
        }
    }
}
