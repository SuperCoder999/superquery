using System;
using System.Threading.Tasks;
using SuperQuery.Services;
using SuperQuery.Models;

namespace SuperQuery.Commands
{
    [Command(6, "getUserInfo")]
    [UseParameter("userId", "userId", "invalidNumber")]
    internal sealed class GetUserInfo : BaseCommand<GetUserInfo>
    {
        private readonly UserService userService = new UserService();

        public override async Task Invoke()
        {
            int userId = GetParameterValue<int>("userId");
            UserAdditionalInfo info = await userService.GetAdditionalInfo(userId);

            Console.WriteLine($"User: {info.User.FirstName} {info.User.LastName}");
            Console.WriteLine($"Last project: {(info.LastProject.HasValue ? info.LastProject.Value.Name : "No")}");
            Console.WriteLine($"Last projects tasks count: {info.LastProjectTasksCount}");
            Console.WriteLine($"Unfinished tasks count: {info.UnfinishedTasksCount}");
            Console.WriteLine($"Longest task: {(info.LongestTask.HasValue ? info.LongestTask.Value.Name : "No")}");
        }
    }
}
