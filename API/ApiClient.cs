using System;
using System.Threading.Tasks;

namespace SuperQuery.API
{
    internal sealed class ApiClient : IDisposable
    {
        private readonly IApiProvider provider;

        public ApiClient(IApiProvider provider)
        {
            this.provider = provider;
        }

        public async Task<T> Get<T>(string endpoint)
        {
            return await provider.Get<T>(endpoint);
        }

        public void Dispose()
        {
            provider.Dispose();
        }
    }
}
