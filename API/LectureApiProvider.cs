using System;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace SuperQuery.API
{
    internal sealed class LectureApiProvider : IApiProvider
    {
        private static readonly string BaseUrl = "https://bsa21.azurewebsites.net/api/";
        private readonly HttpClient client = new HttpClient();

        public LectureApiProvider()
        {
            client.BaseAddress = new Uri(BaseUrl);
        }

        public async Task<T> Get<T>(string endpoint)
        {
            HttpResponseMessage response = await client.GetAsync(endpoint);
            ThrowIfResponseNotOk(response);
            string stringBody = await response.Content.ReadAsStringAsync();
            T result = JsonConvert.DeserializeObject<T>(stringBody);

            return result;
        }

        public void Dispose()
        {
            client.Dispose();
        }

        private void ThrowIfResponseNotOk(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                throw new InvalidOperationException($"{response.StatusCode}: {response.ReasonPhrase}");
            }
        }
    }
}
