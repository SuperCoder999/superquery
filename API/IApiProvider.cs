using System;
using System.Threading.Tasks;

namespace SuperQuery.API
{
    public interface IApiProvider : IDisposable
    {
        Task<T> Get<T>(string endpoint);
    }
}
