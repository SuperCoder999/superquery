﻿using System.Threading.Tasks;
using SuperQuery.ConsoleInterface;
using SuperQuery.Data;

namespace SuperQuery
{
    public sealed class Program
    {
        public static async Task Main(string[] args)
        {
            Database.SetProvider<LectureDatabaseProvider>();

            ConsoleMenu app = new ConsoleMenu();
            await app.Run();
        }
    }
}
