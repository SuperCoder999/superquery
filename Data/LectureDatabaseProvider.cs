using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using SuperQuery.Models;
using SuperQuery.API;

namespace SuperQuery.Data
{
    internal sealed class LectureDatabaseProvider : IDatabaseProvider, IDisposable
    {
        private readonly ApiClient api = new ApiClient(new LectureApiProvider());

        public async Task<List<Project>> GetProjects()
        {
            List<Project> projects = await GetRawProjects();

            if (projects.Count == 0)
            {
                return new List<Project>();
            }

            List<User> users = await GetUsers();
            List<Team> teams = await GetJustTeams(users);
            List<TaskModel> tasks = await GetJustTasks(users);

            projects = JoinProjects(projects, teams, users, tasks).ToList();
            return projects;
        }

        public async Task<List<Team>> GetTeams()
        {
            List<User> users = await GetUsers();

            return await GetJustTeams(users);
        }

        public async Task<List<User>> GetUsers()
        {
            return await GetRawUsers();
        }

        public async Task<List<TaskModel>> GetTasks()
        {
            List<User> users = await GetUsers();

            return await GetJustTasks(users);
        }

        public async Task<List<Project>> GetRawProjects()
        {
            return await api.Get<List<Project>>("Projects");
        }

        public async Task<List<Team>> GetRawTeams()
        {
            return await api.Get<List<Team>>("Teams");
        }

        public async Task<List<User>> GetRawUsers()
        {
            return await api.Get<List<User>>("Users");
        }

        public async Task<List<TaskModel>> GetRawTasks()
        {
            return await api.Get<List<TaskModel>>("Tasks");
        }

        public async Task<Project> GetProject(int id)
        {
            Project rawProject = await GetRawProject(id);
            Project[] projectAsArray = new Project[] { rawProject };
            List<User> users = await GetUsers();

            Team team = await GetRawTeam(rawProject.TeamId);
            IEnumerable<Team> teamAsArray = JoinTeams(new Team[] { team }, users);

            User author = users.Find(u => u.Id == rawProject.AuthorId);
            User[] authorAsArray = new User[] { author };

            IEnumerable<TaskModel> tasks = JoinTasks(await GetRawTasks(), users);

            return JoinProjects(projectAsArray, teamAsArray, authorAsArray, tasks).First();
        }

        public async Task<Team> GetTeam(int id)
        {
            Team rawTeam = await GetRawTeam(id);
            Team[] teamAsArray = new Team[] { rawTeam };
            List<User> users = await GetUsers();

            return JoinTeams(teamAsArray, users).First();
        }

        public async Task<User> GetUser(int id)
        {
            return await GetRawUser(id);
        }

        public async Task<TaskModel> GetTask(int id)
        {
            TaskModel rawTask = await GetRawTask(id);
            TaskModel[] taskAsArray = new TaskModel[] { rawTask };

            User performer = await GetUser(rawTask.PerformerId);
            User[] performerAsArray = new User[] { performer };

            return JoinTasks(taskAsArray, performerAsArray).First();
        }

        public async Task<Project> GetRawProject(int id)
        {
            return await api.Get<Project>($"Projects/{id}");
        }

        public async Task<Team> GetRawTeam(int id)
        {
            return await api.Get<Team>($"Teams/{id}");
        }

        public async Task<User> GetRawUser(int id)
        {
            return await api.Get<User>($"Users/{id}");
        }

        public async Task<TaskModel> GetRawTask(int id)
        {
            return await api.Get<TaskModel>($"Tasks/{id}");
        }

        public void Dispose()
        {
            api.Dispose();
        }

        private async Task<List<Team>> GetJustTeams(IEnumerable<User> users)
        {
            List<Team> teams = await api.Get<List<Team>>("Teams");

            teams = JoinTeams(teams, users).ToList();
            return teams;
        }

        private async Task<List<TaskModel>> GetJustTasks(IEnumerable<User> users)
        {
            List<TaskModel> tasks = await api.Get<List<TaskModel>>("Tasks");

            tasks = JoinTasks(tasks, users).ToList();
            return tasks;
        }

        private IEnumerable<TaskModel> JoinTasks(IEnumerable<TaskModel> tasks, IEnumerable<User> users)
        {
            return tasks
                .Join<TaskModel, User, int, TaskModel>(
                    users,
                    t => t.PerformerId,
                    u => u.Id,
                    (t, u) => new TaskModel
                    {
                        Id = t.Id,
                        ProjectId = t.ProjectId,
                        PerformerId = t.PerformerId,
                        Performer = u,
                        Name = t.Name,
                        Description = t.Description,
                        State = t.State,
                        CreatedAt = t.CreatedAt,
                        FinishedAt = t.FinishedAt,
                    }
                );
        }

        private IEnumerable<Team> JoinTeams(IEnumerable<Team> teams, IEnumerable<User> users)
        {
            return teams
                .GroupJoin<Team, User, int, Team>(
                    users,
                    t => t.Id,
                    u => u.TeamId.HasValue ? u.TeamId.Value : -1,
                    (t, users) => new Team
                    {
                        Id = t.Id,
                        Name = t.Name,
                        CreatedAt = t.CreatedAt,
                        Users = users,
                    }
                );
        }

        private IEnumerable<Project> JoinProjects(
            IEnumerable<Project> projects,
            IEnumerable<Team> teams,
            IEnumerable<User> users,
            IEnumerable<TaskModel> tasks
        )
        {
            return projects
                .GroupJoin<Project, TaskModel, int, Project>(
                    tasks,
                    p => p.Id,
                    t => t.ProjectId,
                    (p, tasks) => new Project
                    {
                        Id = p.Id,
                        AuthorId = p.AuthorId,
                        TeamId = p.TeamId,
                        Name = p.Name,
                        Description = p.Description,
                        CreatedAt = p.CreatedAt,
                        Deadline = p.Deadline,
                        Tasks = tasks,
                    }
                )
                .Join<Project, User, int, Project>(
                    users,
                    p => p.AuthorId,
                    u => u.Id,
                    (p, u) => new Project
                    {
                        Id = p.Id,
                        AuthorId = p.AuthorId,
                        Author = u,
                        TeamId = p.TeamId,
                        Name = p.Name,
                        Description = p.Description,
                        CreatedAt = p.CreatedAt,
                        Deadline = p.Deadline,
                        Tasks = p.Tasks,
                    }
                )
                .Join<Project, Team, int, Project>(
                    teams,
                    p => p.TeamId,
                    t => t.Id,
                    (p, t) => new Project
                    {
                        Id = p.Id,
                        AuthorId = p.AuthorId,
                        Author = p.Author,
                        TeamId = p.TeamId,
                        Team = t,
                        Name = p.Name,
                        Description = p.Description,
                        CreatedAt = p.CreatedAt,
                        Deadline = p.Deadline,
                        Tasks = p.Tasks,
                    }
                );
        }
    }
}
