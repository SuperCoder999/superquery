using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SuperQuery.Models;

namespace SuperQuery.Data
{
    internal static class Database
    {
        private static IDatabaseProvider provider;

        public static void SetProvider<TProvider>() where TProvider : class, IDatabaseProvider, new()
        {
            provider = new TProvider();
        }

        public static async Task<List<Project>> GetProjects()
        {
            ThrowIfNoProvider();
            return await provider.GetProjects();
        }

        public static async Task<List<Team>> GetTeams()
        {
            ThrowIfNoProvider();
            return await provider.GetTeams();
        }

        public static async Task<List<User>> GetUsers()
        {
            ThrowIfNoProvider();
            return await provider.GetUsers();
        }

        public static async Task<List<TaskModel>> GetTasks()
        {
            ThrowIfNoProvider();
            return await provider.GetTasks();
        }

        public static async Task<List<Project>> GetRawProjects()
        {
            ThrowIfNoProvider();
            return await provider.GetRawProjects();
        }

        public static async Task<List<Team>> GetRawTeams()
        {
            ThrowIfNoProvider();
            return await provider.GetRawTeams();
        }

        public static async Task<List<User>> GetRawUsers()
        {
            ThrowIfNoProvider();
            return await provider.GetRawUsers();
        }

        public static async Task<List<TaskModel>> GetRawTasks()
        {
            ThrowIfNoProvider();
            return await provider.GetRawTasks();
        }

        public static async Task<Project> GetProject(int id)
        {
            ThrowIfNoProvider();
            return await provider.GetProject(id);
        }

        public static async Task<Team> GetTeam(int id)
        {
            ThrowIfNoProvider();
            return await provider.GetTeam(id);
        }

        public static async Task<User> GetUser(int id)
        {
            ThrowIfNoProvider();
            return await provider.GetUser(id);
        }

        public static async Task<TaskModel> GetTask(int id)
        {
            ThrowIfNoProvider();
            return await provider.GetTask(id);
        }

        public static async Task<Project> GetRawProject(int id)
        {
            ThrowIfNoProvider();
            return await provider.GetRawProject(id);
        }

        public static async Task<Team> GetRawTeam(int id)
        {
            ThrowIfNoProvider();
            return await provider.GetRawTeam(id);
        }

        public static async Task<User> GetRawUser(int id)
        {
            ThrowIfNoProvider();
            return await provider.GetRawUser(id);
        }

        public static async Task<TaskModel> GetRawTask(int id)
        {
            ThrowIfNoProvider();
            return await provider.GetRawTask(id);
        }

        private static void ThrowIfNoProvider()
        {
            if (provider == null)
            {
                throw new InvalidOperationException("No provider set for database");
            }
        }
    }
}
