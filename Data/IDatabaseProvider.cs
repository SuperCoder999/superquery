using System.Threading.Tasks;
using System.Collections.Generic;
using SuperQuery.Models;

namespace SuperQuery.Data
{
    internal interface IDatabaseProvider
    {
        Task<List<Project>> GetProjects();
        Task<List<Team>> GetTeams();
        Task<List<User>> GetUsers();
        Task<List<TaskModel>> GetTasks();

        Task<List<Project>> GetRawProjects();
        Task<List<Team>> GetRawTeams();
        Task<List<User>> GetRawUsers();
        Task<List<TaskModel>> GetRawTasks();

        Task<Project> GetProject(int id);
        Task<Team> GetTeam(int id);
        Task<User> GetUser(int id);
        Task<TaskModel> GetTask(int id);

        Task<Project> GetRawProject(int id);
        Task<Team> GetRawTeam(int id);
        Task<User> GetRawUser(int id);
        Task<TaskModel> GetRawTask(int id);
    }
}
