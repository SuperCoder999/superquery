namespace SuperQuery.Models
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled,
    }
}
