using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SuperQuery.Models
{
    public struct Team
    {
        [JsonRequired]
        public int Id { get; set; }

        public string Name { get; set; }

        [JsonRequired]
        public DateTime CreatedAt { get; set; }

        [JsonIgnore]
        public IEnumerable<User> Users;
    }
}
