#nullable enable

using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SuperQuery.Models
{
    public struct User
    {
        [JsonRequired]
        public int Id { get; set; }

        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        [JsonRequired]
        public DateTime RegisteredAt { get; set; }

        [JsonRequired]
        public DateTime BirthDay { get; set; }

        [JsonIgnore]
        public IEnumerable<TaskModel> Tasks;
    }
}
