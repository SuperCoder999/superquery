#nullable enable

using System;
using Newtonsoft.Json;

namespace SuperQuery.Models
{
    public struct TaskModel // Conflicts with System.Threading.Tasks.Task
    {
        [JsonRequired]
        public int Id { get; set; }

        [JsonRequired]
        public int ProjectId { get; set; }

        [JsonRequired]
        public int PerformerId { get; set; }

        [JsonIgnore]
        public User Performer { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        [JsonRequired]
        public TaskState State { get; set; }

        [JsonRequired]
        public DateTime CreatedAt { get; set; }

        public DateTime? FinishedAt { get; set; }
    }
}
