using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace SuperQuery.Models
{
    public struct Project
    {
        [JsonRequired]
        public int Id { get; set; }

        [JsonRequired]
        public int AuthorId { get; set; }

        [JsonIgnore]
        public User Author { get; set; }

        [JsonRequired]
        public int TeamId { get; set; }

        [JsonIgnore]
        public Team Team { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        [JsonRequired]
        public DateTime Deadline { get; set; }

        [JsonRequired]
        public DateTime CreatedAt { get; set; }

        [JsonIgnore]
        public IEnumerable<TaskModel> Tasks;
    }
}
