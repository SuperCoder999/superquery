using System;
using System.Linq;
using System.Collections.Generic;
using SuperQuery.Commands;

namespace SuperQuery.ConsoleInterface
{
    internal sealed class CommandRenderer
    {
        private readonly List<ICommand> commands;

        public CommandRenderer(List<ICommand> commands)
        {
            this.commands = commands;
        }

        public void RenderCommandsListInConsole()
        {
            Console.WriteLine(GetCommandsString());
        }

        private string GetCommandsString()
        {
            return string.Join("\n", from command in commands select command.ToString());
        }
    }
}
