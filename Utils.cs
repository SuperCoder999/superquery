using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace SuperQuery
{
    internal static class Utils
    {
        public static TAttr GetAttribute<T, TAttr>(bool inherit = false)
            where T : class
            where TAttr : Attribute
        {
            TAttr attribute = typeof(T).GetCustomAttribute<TAttr>(inherit);

            return attribute;
        }

        public static TAttr GetAttribute<TAttr>(Type type, bool inherit = false) where TAttr : Attribute
        {
            TAttr attribute = type.GetCustomAttribute<TAttr>(inherit);

            return attribute;
        }

        public static List<TAttr> GetAttributes<T, TAttr>(bool inherit = false)
            where T : class
            where TAttr : Attribute
        {
            IEnumerable<TAttr> attributes = typeof(T).GetCustomAttributes<TAttr>(inherit);

            return attributes.ToList();
        }

        public static List<TAttr> GetAttributes<TAttr>(Type type, bool inherit = false) where TAttr : Attribute
        {
            IEnumerable<TAttr> attributes = type.GetCustomAttributes<TAttr>(inherit);

            return attributes.ToList();
        }
    }
}
